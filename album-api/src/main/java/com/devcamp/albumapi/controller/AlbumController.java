package com.devcamp.albumapi.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.albumapi.model.Album;
import com.devcamp.albumapi.repository.AlbumRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class AlbumController {
    @Autowired
    AlbumRepository albumRepository;

    @GetMapping("/album/all")
    public List<Album> getAllAlbum() {
        return albumRepository.findAll();

    }

    @PostMapping("/album/create")
    public ResponseEntity<Album> createAlbum(@Valid @RequestBody Album pAlbum) {
        try {
            Album cAlbum = new Album();
            cAlbum.setAlbumCode(pAlbum.getAlbumCode());
            cAlbum.setAlbumName((pAlbum.getAlbumName()));
            cAlbum.setDescriptAlbum(pAlbum.getDescriptAlbum());
            cAlbum.setCreateAlbum(new Date());
            cAlbum.setImages(pAlbum.getImages());
            return new ResponseEntity<>(albumRepository.save(cAlbum), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/album/update/{id}")
    public ResponseEntity<Album> updateAlbum(@PathVariable("id") long id, @RequestBody Album pAlbum) {
        Optional<Album> cOptional = albumRepository.findById(id);
        if (cOptional.isPresent()) {
            try {
                cOptional.get().setAlbumCode(pAlbum.getAlbumCode());
                cOptional.get().setAlbumName(pAlbum.getAlbumName());
                cOptional.get().setDescriptAlbum(pAlbum.getDescriptAlbum());
                cOptional.get().setImages(pAlbum.getImages());
                cOptional.get().setCreateAlbum(new Date());
                return new ResponseEntity<>(albumRepository.save(cOptional.get()), HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping("/album/delete/{id}")
    public ResponseEntity<Album> deleteAlbum(@PathVariable("id") long id) {
        albumRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/album/details/{id}")
    public Album getAlbumById(@PathVariable("id") long id) {
        return albumRepository.findById(id).get();
    }

}
