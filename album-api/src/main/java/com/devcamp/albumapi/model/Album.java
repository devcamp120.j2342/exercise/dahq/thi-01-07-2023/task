package com.devcamp.albumapi.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "albums")
public class Album {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotNull(message = "Nhập ma album")
    @Size(min = 3, message = "Mã album phải có ít nhất 2 ký tự ")

    @Column(name = "album_code", unique = true, nullable = false)
    private String albumCode;

    @Column(name = "album_name")
    private String albumName;
    @Column(name = "descript_album")
    private String descriptAlbum;
    @Column(name = "create_album")
    private Date createAlbum;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "album")

    private Set<Image> images;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAlbumCode() {
        return albumCode;
    }

    public void setAlbumCode(String albumCode) {
        this.albumCode = albumCode;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getDescriptAlbum() {
        return descriptAlbum;
    }

    public void setDescriptAlbum(String descriptAlbum) {
        this.descriptAlbum = descriptAlbum;
    }

    public Date getCreateAlbum() {
        return createAlbum;
    }

    public void setCreateAlbum(Date createAlbum) {
        this.createAlbum = createAlbum;
    }

    public Album(long id, String albumCode, String albumName, String descriptAlbum, Date createAlbum,
            Set<Image> images) {
        this.id = id;
        this.albumCode = albumCode;
        this.albumName = albumName;
        this.descriptAlbum = descriptAlbum;
        this.createAlbum = createAlbum;
        this.images = images;
    }

    public Album() {
    }

    public Set<Image> getImages() {
        return images;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }

}
