-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2023 at 01:10 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dum_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` bigint(20) NOT NULL,
  `album_code` varchar(255) NOT NULL,
  `album_name` varchar(255) DEFAULT NULL,
  `create_album` datetime DEFAULT NULL,
  `descript_album` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `album_code`, `album_name`, `create_album`, `descript_album`) VALUES
(2, 'Al1', 'Album1', '2023-07-13 18:03:12', 'Laptopp'),
(3, 'Al2', 'Album2', '2023-07-13 18:02:29', 'Phone'),
(4, 'Al3', 'Album3', '2023-07-13 18:02:58', 'Book');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) NOT NULL,
  `create_image` datetime DEFAULT NULL,
  `descript_image` varchar(255) DEFAULT NULL,
  `image_code` varchar(255) NOT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `url_image` varchar(255) DEFAULT NULL,
  `album_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `create_image`, `descript_image`, `image_code`, `image_name`, `url_image`, `album_id`) VALUES
(2, '2023-07-13 18:04:14', 'Dell1', 'Imagg1', 'Imagee1', '//////', 4),
(3, '2023-07-13 18:04:43', 'acer1', 'Ima2', 'Image2', '//////', 2),
(5, '2023-07-13 18:08:19', 'Samsung', 'Imag4', 'Imaage4', '', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_h7tyk8kn08kwun4pk76r2dy11` (`album_code`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_niop0u4v6svl8sblkt7ulxp4b` (`image_code`),
  ADD KEY `FK724cv7ds8vwsp7mpi6e5s7keq` (`album_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `FK724cv7ds8vwsp7mpi6e5s7keq` FOREIGN KEY (`album_id`) REFERENCES `albums` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
